import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { environment } from '../../../environments/environment';
import { Tweet } from './tweet';
import { Interest } from '../../shared/model/interest';

@Injectable()
export class InterestService {
  constructor(private http: HttpClient) { }

  getTweetsOfInterest(interestId: string): Observable<Tweet[]>{
    return this.http.get<Tweet[]>(environment.apiUrl + 'interests/' + interestId + '/tweets');
  }

  getTweetsOfAllInterests(interests: Interest[]): Observable<Tweet[]>{
    return this.http.post<Tweet[]>(environment.apiUrl + 'interests/tweets',interests);
  }

}
