export class Tweet {
  text: string;
  user: string;
  createdAt: Date;
}
