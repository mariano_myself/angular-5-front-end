import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OnDestroy } from "@angular/core";
import "rxjs/add/operator/takeUntil";
import { Subject } from "rxjs/Subject";

import { Interest } from '../../shared/model/interest';
import { UserService } from '../../shared/service/user.service';

import swal from 'sweetalert2';

@Component({
  selector: 'app-interest-form',
  templateUrl: './interest-form.component.html',
  styleUrls: ['./interest-form.component.css']
})
export class InterestFormComponent implements OnDestroy, OnInit {
  interest = new Interest();
  interests: Interest[];
  private unsubscribe = new Subject<void>();

  constructor(private router: Router, private userService:UserService) {
    this.interest.type = "Hashtag";
  }

  ngOnInit() {
    this.userService.getInterests()
    .takeUntil(this.unsubscribe)
    .subscribe(
      result => {
        this.interests = result;
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  addInterest() {
    this.userService.addInterest(this.interest)
    .takeUntil(this.unsubscribe)
    .subscribe(
      result => {
        this.router.navigate(['/home']);
      },
      error => {
        console.log(error);
        swal({
          type: 'error',
          title: 'Invalid Interest',
          text: error.error.message,
        })
      }
    );
  }

  removeInterest(interestId: string) {
    this.userService.removeInterest(interestId)
    .takeUntil(this.unsubscribe)
    .subscribe(
      result => {
        this.ngOnInit();
      },
      error => {
        console.log(<any>error);
      }
    );
  }

  public ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }

}
