import { Interest } from '../shared/model/interest';

export class InterestChecked {
  interest: Interest;
  check: boolean;
  constructor(interest: Interest, check: boolean) {
    this.interest = interest;
    this.check = check;
  }
}
