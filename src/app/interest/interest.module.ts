import { NgModule }       from '@angular/core';
import { CommonModule }   from '@angular/common';
import { FormsModule }    from '@angular/forms';

import { InterestFormComponent } from './interest-form/interest-form.component';
import { InterestService } from './shared/interest.service';

import { InterestRoutingModule } from './interest-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    InterestRoutingModule
  ],
  declarations: [

    InterestFormComponent
  ],
  providers: [InterestService]
})
export class InterestModule { }
