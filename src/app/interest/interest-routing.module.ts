import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from '../home/home.component';
import { InterestFormComponent } from './interest-form/interest-form.component';

const interestRoutes: Routes = [
  { path: 'manage', component: InterestFormComponent }
];

@NgModule({
  imports: [
    RouterModule.forChild(interestRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class InterestRoutingModule { }
