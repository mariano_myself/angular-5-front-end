import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { Router } from '@angular/router';
import { OnDestroy } from "@angular/core";
import "rxjs/add/operator/takeUntil";
import { Subject } from "rxjs/Subject";
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';

import { Interest } from '../../shared/model/interest';
import { InterestChecked } from '../interestChecked';
import { InterestService } from '../shared/interest.service';
import { UserService } from '../../shared/service/user.service';
import { Tweet } from '../shared/tweet';

import swal from 'sweetalert2';

@Component({
  selector: 'app-interest-list',
  templateUrl: './interest-list.component.html',
  styleUrls: ['./interest-list.component.css']
})
export class InterestListComponent implements OnDestroy, OnInit {
  currentPage:number = 0;
  pageSize:number = 10;
  loading = false;
  showPagination = false;
  checkedList: InterestChecked[];
  tweets: Tweet[];
  interests: Interest[];
  interestsToSearchList: Interest[];
  private unsubscribe = new Subject<void>();

  constructor(private router: Router, private activatedRoute: ActivatedRoute,
    private interestService: InterestService,
    private userService: UserService,
    private spinnerService: Ng4LoadingSpinnerService) {
      this.checkedList = [];
    }

    ngOnInit() {
      this.lockPage();
      this.userService.getInterests().subscribe(
        interests => {
          if (interests.length === 0) this.router.navigate(['/interest/manage']);
          else {
            this.findInterests(interests);
          }
        }
      );
    }

    refreshListOfTweets() {
      this.lockPage();
      this.interestsToSearchList = this.checkedList
      .filter(interestChecked => interestChecked.check === true)
      .map(interestChecked => interestChecked.interest);
      if (this.interestsToSearchList.length === 0) {
        this.spinnerService.hide();
        swal({
          type: 'warning',
          title: 'Please check at least one Interest',
          text: 'You don´t have any interest to see',
        })
        this.tweets = [];
      }
      else {
        this.findTweets(this.interestsToSearchList);
      }
    }

    seeTweetsOfAllInterests() {
      this.checkedList = this.checkedList
      .map(interestChecked => new InterestChecked(interestChecked.interest, true));
      this.refreshListOfTweets();
    }

    private connectionError() {
      this.spinnerService.hide();
      swal({
        type: 'error',
        title: 'Cannot connect to Twitter',
        text: 'Please wait a few minutes',
      })
    }

    private findTweets(interests: Interest[]) {
      this.interestService.getTweetsOfAllInterests(interests)
      .takeUntil(this.unsubscribe)
      .subscribe(
        result => {
          this.tweets = result;
          this.unlockPage();
        },
        error => {
          this.connectionError();
          console.log(<any>error);
        }
      );
    }

    private findInterests(interests: Interest[]) {
      this.interests = interests;
      this.checkedList = this.interests
      .map(interest => new InterestChecked(interest, true));
      this.userService.getInterests()
      .takeUntil(this.unsubscribe)
      .subscribe(
        result => {
          this.findTweets(result);
        },
        error => {
          console.log(<any>error);
        }
      );
    }

    private lockPage() {
      this.showPagination = false;
      this.spinnerService.show();
    }

    private unlockPage () {
      this.spinnerService.hide();
      this.showPagination = true;
    }

    public ngOnDestroy() {
      this.unsubscribe.next();
      this.unsubscribe.complete();
    }

  }
