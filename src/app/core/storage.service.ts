import { Injectable } from "@angular/core";
import {Observable} from 'rxjs/Observable';
import { Session } from "../shared/model/session";
import { User } from "../shared/model/user";

@Injectable()
export class StorageService {
  private localStorageService;
  private currentSession : Session = null;
  constructor() {
    this.localStorageService = localStorage;
    this.currentSession = this.loadSessionData();
  }
  setCurrentSession(session: Session): void {
    this.currentSession = session;
    this.localStorageService.setItem('currentUser', JSON.stringify(session));
  }
  loadSessionData(): Session{
    var sessionStr = this.localStorageService.getItem('currentUser');
    return (sessionStr) ? <Session> JSON.parse(sessionStr) : null;
  }
  getCurrentSession(): Session {
    return this.currentSession;
  }
  removeCurrentSession(): void {
    this.localStorageService.removeItem('currentUser');
  }
  getCurrentUser(): User {
    var session: Session = this.getCurrentSession();
    return (session && session.user) ? session.user : null;
  };
  isAuthenticated(): Observable<boolean> {
    return (this.getCurrentToken() != null) ? Observable.of(true) : Observable.of(false);
  };
  getCurrentToken(): string {
    var session = this.getCurrentSession();
    return (session && session.token) ? session.token : null;
  };
  logout(): void{
    this.removeCurrentSession();
  }
}
