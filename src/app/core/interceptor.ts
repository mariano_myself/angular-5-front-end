import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { StorageService } from '../core/storage.service';
import { Observable } from 'rxjs/Observable';
import { environment } from '../../environments/environment';

@Injectable()
export class Interceptor implements HttpInterceptor {
  constructor(private storageService: StorageService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (request.url === environment.apiUrl + 'login' || request.url === environment.apiUrl + 'sign-up' ) {
      request = request.clone();
    } else {
      let headers = request.headers
      .set('Content-Type', 'application/json')
      .set('Authorization', `Bearer ${this.storageService.getCurrentToken()}`);
      request = request.clone({ headers });
    }
    return next.handle(request);
  }
}
