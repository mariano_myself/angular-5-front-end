import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { OnDestroy } from "@angular/core";
import "rxjs/add/operator/takeUntil";
import { Subject } from "rxjs/Subject";

import { AuthenticationService } from '../shared/authentication.service';
import { StorageService } from '../../core/storage.service';
import { User } from '../../shared/model/user';

import swal from 'sweetalert2';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnDestroy, OnInit {
  user = new User();
  private unsubscribe = new Subject<void>();

  constructor(private router: Router,
    private storageService: StorageService,
    private authenticationService: AuthenticationService) { }

  ngOnInit() {
    this.storageService.logout();
  }

  signUp() {
    this.authenticationService.signUp(this.user)
      .takeUntil(this.unsubscribe)
      .subscribe(
            result => {
              this.router.navigate(['/registry/login']);
            },
            error => {
              console.log(error);
              swal({
                type: 'error',
                title: 'Invalid Register',
                text: error.error.message,
              })
            }
        );
  }

  public ngOnDestroy() {
    this.unsubscribe.next();
    this.unsubscribe.complete();
  }
}
