import { Component, OnInit } from '@angular/core';
import { OnDestroy } from "@angular/core";
import "rxjs/add/operator/takeUntil";
import { Subject } from "rxjs/Subject";
import { Router } from '@angular/router';

import { AuthenticationService } from '../shared/authentication.service';
import { LoginResponse } from '../model/loginResponse';
import { Session } from '../../shared/model/session';
import { StorageService } from '../../core/storage.service';
import { User } from '../../shared/model/user';
import { UserService } from '../../shared/service/user.service';

import swal from 'sweetalert2';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnDestroy, OnInit {

  model: any = {};
  loading = false;
  error = '';
  private unsubscribe = new Subject<void>();

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private storageService: StorageService,
    private userService:UserService) { }

    ngOnInit() {
      this.storageService.logout();
    }

    login() {
      this.loading = true;
      this.authenticationService.login(this.model.username, this.model.password)
      .takeUntil(this.unsubscribe)
      .subscribe(
        data =>  {
          this.correctLogin({token: data.token, user: data.user});
        },
        error => {
          swal({
            type: 'error',
            title: 'Invalid Login',
            text: error.error.message,
          })
          this.loading = false;
        }
      );
    }

    private correctLogin(session: Session) {
      this.storageService.setCurrentSession(session);
      this.loading = false;
      this.router.navigate(['/home']);
    }

    signUp() {
      this.router.navigate(["/registry/sign-up"]);
    }

    public ngOnDestroy() {
      this.unsubscribe.next();
      this.unsubscribe.complete();
    }
  }
