import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/operators'

import { LoginResponse } from '../model/loginResponse';
import { environment } from '../../../environments/environment';

@Injectable()
export class AuthenticationService {

  constructor(private http: HttpClient) {}

  login(username: string, password: string): Observable<LoginResponse>{
    const headers = new HttpHeaders();
    return this.http.post<LoginResponse>(environment.apiUrl + 'login', { username: username, password: password });
  }

  signUp(user: any){
    const headers = new HttpHeaders();
    headers.append('Content-Type', 'application/json');
    return this.http.post(environment.apiUrl + 'sign-up', user, {headers: headers});
  }
}
