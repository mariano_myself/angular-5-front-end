import { User } from '../../shared/model/user';

export class LoginResponse {
  user: User;
  token: string;
}
