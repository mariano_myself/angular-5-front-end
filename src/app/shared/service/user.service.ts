import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'

import { environment } from '../../../environments/environment';
import { Interest } from '../model/interest';
import { StorageService } from '../../core/storage.service';
import { User } from '../model/user';

@Injectable()
export class UserService {
  constructor(
    private http: HttpClient, private storageService: StorageService) {
    }

    getUser(): Observable<User> {
      return this.http.get<User>(environment.apiUrl + 'user/' + this.storageService.getCurrentUser().id);
    }

    getInterests(): Observable<Interest[]> {
      return this.http.get<Interest[]>(environment.apiUrl + 'user/' + this.storageService.getCurrentUser().id + '/interests');
    }

    addInterest(interest: Interest): Observable<Interest> {
      return this.http.post<Interest>(environment.apiUrl + 'user/' + this.storageService.getCurrentUser().id + '/interest', interest);
    }

    removeInterest(interestId: string) {
      return this.http.delete(environment.apiUrl + 'user/' + this.storageService.getCurrentUser().id + '/interest/' + interestId);
    }
  }
