import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxPaginationModule } from 'ngx-pagination';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';

import {InterestModule} from '../interest/interest.module';

import { HomeComponent } from './home.component';
import { InterestListComponent } from '../interest/interest-list/interest-list.component';


import { HomeRoutingModule } from './home-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgxPaginationModule,
    Ng4LoadingSpinnerModule.forRoot(),
    InterestModule,
    HomeRoutingModule
  ],
  declarations: [
    HomeComponent,
    InterestListComponent
  ],
  providers: []
})
export class HomeModule { }
