import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from './core/authGuard';
import { HomeLayoutComponent } from './layouts/home-layout/home-layout.component';
import { LoginLayoutComponent } from './layouts/login-layout/login-layout.component';

const appRoutes: Routes = [
  {
    path: '',
    component: HomeLayoutComponent,
    canActivate: [AuthGuard],
    children: [
      {
        path: 'home',
        loadChildren: './home/home.module#HomeModule'
      },
      {
        path: 'interest',
        loadChildren: './interest/interest.module#InterestModule'
      },
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'home'
      },
    ]
  },
  {
    path: '',
    component: LoginLayoutComponent,
    children: [
      {
        path: 'registry',
        loadChildren: './authentication/authentication.module#AuthenticationModule'
      }
    ]
  },
  { path: '**', redirectTo: 'home' }
];




//   {
//     path: 'registry',
//     loadChildren: 'app/authentication/authentication.module#AuthenticationModule'
//   },
//   {
//     path: '',
//     loadChildren: './home/home.module#HomeModule'
//   }
// ];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes
    )
  ],
  exports: [
    RouterModule
  ],
  providers: []
})
export class AppRoutingModule { }
